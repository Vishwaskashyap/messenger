package com.example.dell.messenger;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button button01 = (Button) findViewById(R.id.button_send);
                final TextView text01 = (TextView) findViewById(R.id.text_message);

        button01.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                text01.setText("sent");
            }
        });

        Button button02 = (Button) findViewById(R.id.button_receive);
        final TextView text02 = (TextView) findViewById(R.id.text_message);

        button02.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                text02.setText("Received");
            }
        });

        Button button03 = (Button) findViewById(R.id.button_hold);
        final TextView text03 = (TextView) findViewById(R.id.text_message);

        button03.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                text03.setText("Drafted");
            }
        });
    }
}
